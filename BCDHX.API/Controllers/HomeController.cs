﻿using BCDHX.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BCDHX.API.Controllers
{
    [RoutePrefix("api/home")]
    public class HomeController : ApiController
    {
        private readonly BCDHXDB _db;
        public HomeController()
        {
            _db = new BCDHXDB();
        }
        [Route("banner/{id}")]
        [HttpGet]
        public IHttpActionResult GetBanners(string id)
        {
            var temp = _db.Silders.Select(x => x).Where(x => x.Title == id).ToList();
            if (temp.Any())
            {
                return Ok(temp);
            }  
            else
            {
                return BadRequest("Not Found");
            }
        }
    }
}
